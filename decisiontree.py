# decisiontree.py
"""Predict Parkinson's disease based on dysphonia measurements using a decision tree."""
"""Alexander Faus"""
import os
import sys
import numpy as np
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix
import graphviz

# Relevant directories and filenames
root = '~/Documents/Fall 2018/Machine Learning/homework-2/data/' #edit as needed


# Load data from relevant files
trainingdata = np.loadtxt("data/trainingdata.txt", dtype=object, delimiter=',')
traininglabels = np.loadtxt("data/traininglabels.txt")


# Train a decision tree via information gain on the training data
decision_tree = tree.DecisionTreeClassifier(criterion="entropy")
decision_tree = decision_tree.fit(trainingdata, traininglabels)

# Test the decision tree
testingdata = np.loadtxt("data/testingdata.txt", dtype=object, delimiter=',')
testinglabels = np.loadtxt("data/testinglabels.txt")


training_accuracy = 0.0
prediction_array = np.zeros(testinglabels.size)
for x in range(0, traininglabels.size):
    prediction = decision_tree.predict([trainingdata[x]])
    if prediction == traininglabels[x]:
        training_accuracy += 1.0

training_accuracy /= float(traininglabels.size)
training_accuracy *= 100.0

testing_accuracy = 0.0
prediction_array = np.zeros(testinglabels.size)
for x in range(0, testinglabels.size):
    prediction = decision_tree.predict([testingdata[x]])
    if prediction == testinglabels[x]:
        testing_accuracy += 1.0
    prediction_array[x] = prediction

testing_accuracy /= float(testinglabels.size)
testing_accuracy *= 100.0

print("Training Accuracy: " + str(training_accuracy) + "%")
print("Testing Accuracy: " + str(testing_accuracy) + "%")

# Compute the confusion matrix on test data
print("Confusion Matrix:")
print(confusion_matrix(testinglabels, prediction_array))

# Visualize the tree using Graphviz
tree.export_graphviz(decision_tree, out_file='tree.dot', class_names=("Does not have Parkinson's",
                                                                      "Does have Parkinson's"),
                     feature_names=(np.loadtxt("data/attributes.txt", dtype=object)), filled=True, rounded=True)