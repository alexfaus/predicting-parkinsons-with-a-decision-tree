# Predicting Parkinsons with a Decision Tree

This project builds and then uses a decision tree to predict whether someone has Parkinsonís disease based on dysphonia measurements. This was a class assignment for CSC3520fa2018 at Florida Southern College. 


## Data Files

* `attributes.txt` - List of dysphonia measurement variables used for classification.
* `trainingdata.txt` and `testingdata.txt` - Dysphonia measurement values for each training/testing sample. Each row is a unique sample containing 22 comma-separated floating-point values.
* `traininglabels.txt` and `testinglabels.txt` - Each line corresponds to a sample from the training/testing set and determines whether that individual is healthy (0) or has Parkinsonís (1).
* `tree.dot` - Graphviz file of the decision tree. To view the graphic, copy and paste the text from the file to: http://webgraphviz.com/ or use `dot -Tpng tree.dot -o tree.png`

There are 185 samples for training and 10 samples for testing. Each sample contains 22 attributes related to dysphonia; 
for details about these measurements, see https://archive.ics.uci.edu/ml/datasets/Parkinsons or [1]

[1] Little, M.A., McSharry, P.E., Hunter, E.J., Spielman, J. and Ramig, L.O., 2009. 
Suitability of dysphonia measurements for telemonitoring of Parkinson's disease. 
*IEEE Transactions on Biomedical Engineering,* 56(4), pp.1015-1022.

### Built With

* [Python 3.7](https://www.python.org/)
* [Sklearn](http://scikit-learn.org/stable/index.html)
* [Graphviz](https://www.graphviz.org/)
* [numpy](http://www.numpy.org/)


## Authors

* **Alexander Faus** 


## Acknowledgments

* Thank you to Professor Matthew Eicholtz at Florida Southern College for project outline


